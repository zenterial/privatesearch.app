install dependencies
~~~~~~~~~~~~~~~~~~~~

run this command in the directory ``searx/static/themes/oscar-mod``

``npm install``

compile sources
~~~~~~~~~~~~~~~

run this command in the directory ``searx/static/themes/oscar-mod``

``grunt``

or in the root directory:

``make grunt``
