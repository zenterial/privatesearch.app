#!/bin/bash

[ ! -d "searx" ] && git clone https://github.com/searx/searx.git
cp -R templates/oscar-mod searx/searx/templates/
cp -R themes/oscar-mod searx/searx/static/themes/
cp -R settings.yml searx/searx/settings.yml
cp -R templates/__common__/about.html searx/searx/templates/__common__/about.html

cd searx && make docker
docker tag searx/searx repo.treescale.com/zenterial/searx

