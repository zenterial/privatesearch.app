This repository contains the modifications and manifests for building and deploying a slightly customized SearX instance as seen at https://privatesearch.app and https://srx.app.

To build a custom container go to the docker-build directory and check the ```build.sh``` file to modify as needed (e.g. the docker image name), then run it and push the image.

To deploy on kubernetes go to the kubernetes directory and apply the manifests. You need to create a configmap for filtron rules.json, and a secret with morty_key you generated. Also change domain names in ingress.yaml.

**TODO**

* Create a helm chart
* Deployment of the admin UI
* Further customizations of the searx UI?

**Credit**

Kubernetes manifests are based on work by [Travis Newman](https://github.com/travnewmatic/k8s-searx).

Original complete SearX source code is at [https://github.com/searx/searx](https://github.com/searx/searx), which is being pulled before applying modifications.
